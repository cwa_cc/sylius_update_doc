<?php

declare(strict_types=1);

namespace App\Entity\Page\Backend;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use App\Entity\Page\Backend\ActualityCategory;
use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TranslatableTrait;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Page\Backend\ActualityArticleTranslation;
use Sylius\Component\Resource\Model\TranslationInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;
use Sylius\Component\Core\Model\ImagesAwareInterface;
use Sylius\Component\Core\Model\ImageInterface;


#[ORM\Entity]
#[ORM\Table(name: 'app_actuality_article')]
class ActualityArticle implements ResourceInterface, TranslatableInterface, ImagesAwareInterface
{
    use TranslatableTrait {
        __construct as private initializeTranslationsCollection;
        getTranslation as defaultGetTranslation;
    }

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: 'boolean')]
    private $enabled;

    #[ORM\Column(type: 'datetime')]
    private $created_at;

    #[ORM\Column(type: 'datetime')]
    private $updated_at;

    #[ORM\ManyToMany(targetEntity: ActualityCategory::class, inversedBy: 'articles')]
    #[Assert\NotBlank(groups: ['sylius'])]
    // #[ORM\JoinColumn(nullable: false)]
    private $category;

    /** @var ArrayCollection<int, ActualityArticleTranslation>|ActualityArticleTranslation[] */
    #[ORM\OneToMany(mappedBy: 'translatable', targetEntity: ActualityArticleTranslation::class, indexBy: 'locale', cascade: ['persist', 'remove'])]
    #[Assert\Valid(groups: ['sylius'])]
    protected $translations;

    #[ORM\OneToMany(mappedBy: 'owner', targetEntity: ActualityArticleImage::class, cascade: ['persist', 'remove'])]
    // #[Assert\Valid(groups: ['sylius'])]
    protected $images;

    public function __construct()
    {
        $this->initializeTranslationsCollection();
        $this->translations = new ArrayCollection();
        $this->category     = new ArrayCollection();
        // /** @var ArrayCollection<array-key, ImageInterface> $this->images */
        $this->images       = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->getTranslation()->getName();
    }

    public function setName(?string $name): void
    {
        $this->getTranslation()->setName($name);
    }

    public function getSlug(): ?string
    {
        return $this->getTranslation()->getSlug();
    }

    public function setSlug(string $slug): void
    {
        $this->getTranslation()->setSlug($slug);
    }

    public function getDescription(): ?string
    {
        return $this->getTranslation()->getDescription();
    }

    public function setDescription(string $description): void
    {
        $this->getTranslation()->setDescription($description);
    }

    public function getMetaDescription(): ?string
    {
        return $this->getTranslation()->getMetaDescription();
    }

    public function setMetaDescription(string $meta_description): void
    {
        $this->getTranslation()->setMetaDescription($meta_description);
    }

    public function getBannerTitle(): ?string
    {
        return $this->getTranslation()->getBannerTitle();
    }

    public function setBannerTitle(string $bannerTitle): void
    {
        $this->getTranslation()->setBannerTitle($bannerTitle);
    }

    public function getBannerAlt(): ?string
    {
        return $this->getTranslation()->getBannerAlt();
    }

    public function setBannerAlt(string $bannerAlt): void
    {
        $this->getTranslation()->setBannerAlt($bannerAlt);
    }

    protected function createTranslation(): TranslationInterface
    {
        return new ActualityArticleTranslation();
    }

    public function getTranslation(?string $locale = null): ActualityArticleTranslation
    {
        /** @var ActualityArticleTranslation $translation */
        $translation = $this->defaultGetTranslation($locale);

        return $translation;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection|ActualityCategory[]
     */
    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function addCategory(ActualityCategory $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    public function removeCategory(ActualityCategory $category): self
    {
        $this->category->removeElement($category);

        return $this;
    }

    public function getImages(): Collection
    {
        // dump($this->images);
        return $this->images;
    }

    public function getImagesByType(string $type): Collection
    {
        return $this->images->filter(function (ImageInterface $image) use ($type): bool {
            return $type === $image->getType();
        });
    }

    public function hasImages(): bool
    {
        return !$this->images->isEmpty();
    }

    public function hasImage(ImageInterface $image): bool
    {
        return $this->images->contains($image);
    }

    public function addImage(ImageInterface $image): void
    {
        // dd($image);
        $image->setOwner($this);
        $this->images->add($image);
    }

    public function removeImage(ImageInterface $image): void
    {
        if ($this->hasImage($image)) {
            $image->setOwner(null);
            $this->images->removeElement($image);
        }
    }
}
