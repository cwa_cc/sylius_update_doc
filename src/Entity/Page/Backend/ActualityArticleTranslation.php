<?php

declare(strict_types=1);

namespace App\Entity\Page\Backend;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\AbstractTranslation;
use Sylius\Component\Resource\Model\TranslatableInterface;

#[ORM\Entity]
#[ORM\Table(name: 'app_actuality_article_translation')]
class ActualityArticleTranslation extends AbstractTranslation implements ResourceInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(groups: ['sylius'])]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(groups: ['sylius'])]
    private ?string $slug = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank(groups: ['sylius'])]
    private ?string $description = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank(groups: ['sylius'])]
    private ?string $meta_description = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(groups: ['sylius'])]
    private ?string $bannerTitle = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank(groups: ['sylius'])]
    private ?string $bannerAlt = null;

    #[ORM\ManyToOne(targetEntity: ActualityArticle::class, inversedBy: 'translations')]
    #[ORM\JoinColumn(nullable: false)]
    protected ?TranslatableInterface $translatable = null;

    #[ORM\Column(type: Types::STRING, nullable: false)]
    protected ?string $locale = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->meta_description;
    }

    public function setMetaDescription(?string $meta_description): self
    {
        $this->meta_description = $meta_description;

        return $this;
    }

    public function getBannerTitle(): ?string
    {
        return $this->bannerTitle;
    }

    public function setBannerTitle(?string $bannerTitle): self
    {
        $this->bannerTitle = $bannerTitle;

        return $this;
    }

    public function getBannerAlt(): ?string
    {
        return $this->bannerAlt;
    }

    public function setBannerAlt(?string $bannerAlt): self
    {
        $this->bannerAlt = $bannerAlt;

        return $this;
    }
}
