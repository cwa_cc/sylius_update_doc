<?php

declare(strict_types=1);

namespace App\Entity\Page\Backend;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TranslatableTrait;
use Sylius\Component\Resource\Model\TranslationInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;

#[ORM\Entity]
#[ORM\Table(name: 'app_actuality_category')]
class ActualityCategory implements ResourceInterface, TranslatableInterface
{
    use TranslatableTrait {
        __construct as private initializeTranslationsCollection;
        getTranslation as defaultGetTranslation;
    }

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    /** @var ArrayCollection<int, ActualityCategoryTranslation>|ActualityCategoryTranslation[] */
    #[ORM\OneToMany(mappedBy: 'translatable', targetEntity: ActualityCategoryTranslation::class, indexBy: 'locale', cascade: ['persist', 'remove'])]
    #[Assert\Valid(groups: ['sylius'])]
    protected $translations;

    public function __construct()
    {
        $this->initializeTranslationsCollection();
        $this->translations = new ArrayCollection();
        // $this->articles     = new ArrayCollection();
    }

    #[ORM\ManyToMany(targetEntity: ActualityArticle::class, mappedBy: 'category')]
    private $articles;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->getTranslation()->getName();
    }

    public function setName(string $name): void
    {
        $this->getTranslation()->setName($name);
    }

    public function getDescription(): ?string
    {
        return $this->getTranslation()->getDescription();
    }

    public function setDescription(string $description): void
    {
        $this->getTranslation()->setDescription($description);
    }

    protected function createTranslation(): TranslationInterface
    {
        return new ActualityCategoryTranslation();
    }

    public function getTranslation(?string $locale = null): ActualityCategoryTranslation
    {
        /** @var ActualityCategoryTranslation $translation */
        $translation = $this->defaultGetTranslation($locale);

        return $translation;
    }

    /**
     * @return Collection|ActualityArticle[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(ActualityArticle $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->addCategory($this);
        }

        return $this;
    }

    public function removeArticle(ActualityArticle $article): self
    {
        if ($this->articles->removeElement($article)) {
            $article->removeCategory($this);
        }

        return $this;
    }
}
