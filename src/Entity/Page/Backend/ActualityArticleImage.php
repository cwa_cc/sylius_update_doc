<?php

declare(strict_types=1);

namespace App\Entity\Page\Backend;

use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Image;

#[ORM\Entity]
#[ORM\Table(name: 'app_actuality_article_image')]
class ActualityArticleImage extends Image
{
}
