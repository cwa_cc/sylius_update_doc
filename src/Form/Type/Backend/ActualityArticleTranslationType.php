<?php

namespace App\Form\Type\Backend;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;


class ActualityArticleTranslationType extends AbstractResourceType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label'         => 'app.actuality.article.index_name',
                'required'      => true,
            ])
            ->add('slug', TextType::class, [
                'label'         => 'app.actuality.article.title_slug',
            ])
            ->add('description', CKEditorType::class, [
                'label'         => 'app.actuality.article.content',
                'required'      => true,
                'config_name'   => 'admin_config',
            ])
            ->add('metaDescription', TextareaType::class, [
                'label'         => 'app.actuality.article.meta_description',
                'required'      => true,
            ])
            ->add('bannerTitle', TextType::class, [
                'label'         => 'app.actuality.article.banner_title',
                'required'      => true,
            ])
            ->add('bannerAlt', TextType::class, [
                'label'         => 'app.actuality.article.banner_alt',
                'required'      => true,
            ])
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'app_actuality_article_translation';
    }
}
