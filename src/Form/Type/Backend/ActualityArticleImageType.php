<?php

declare(strict_types=1);

namespace App\Form\Type\Backend;

use Sylius\Bundle\CoreBundle\Form\Type\ImageType;

final class ActualityArticleImageType extends ImageType
{
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'app_actuality_article_image';
    }
}
