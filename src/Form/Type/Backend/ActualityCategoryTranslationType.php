<?php

namespace App\Form\Type\Backend;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;

class ActualityCategoryTranslationType extends AbstractResourceType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label'         => 'app.actuality.category.name',
                'required'      => true,
            ])
            ->add('description', CKEditorType::class, [
                'label'         => 'app.actuality.category.description',
                'required'      => true,
                'config_name'   => 'admin_config',
            ])
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'app_actuality_category_translation';
    }
}
