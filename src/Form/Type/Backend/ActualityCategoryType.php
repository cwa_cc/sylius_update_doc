<?php

namespace App\Form\Type\Backend;

use Symfony\Component\Form\FormBuilderInterface;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;

class ActualityCategoryType extends AbstractResourceType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type'    => ActualityCategoryTranslationType::class,
            ])
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'app_actuality_category';
    }
}
