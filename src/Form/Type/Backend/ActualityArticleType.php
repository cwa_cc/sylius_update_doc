<?php

namespace App\Form\Type\Backend;

use DateTimeZone;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use App\Entity\Page\Backend\ActualityCategory;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Form\Type\Backend\ActualityArticleImageType;
use App\Form\Type\Backend\ActualityArticleTranslationType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;

class ActualityArticleType extends AbstractResourceType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type'    => ActualityArticleTranslationType::class,
            ])
            ->add('enabled', CheckboxType::class, [
                'required'      => false,
                'label'         => 'app.general.message.enabled'
            ])
            ->add('category', EntityType::class, [
                'label'         => 'app.actuality.article.category',
                'class'         => ActualityCategory::class,
                'choice_label'  => 'name',
                'multiple'      => true,
                'expanded'      => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.id', 'ASC');
                },
            ])
            ->add('images', CollectionType::class, [
                'label'         => false,
                'entry_type'    => ActualityArticleImageType::class,
                'allow_add'     => true,
                'allow_delete'  => true,
                'by_reference'  => false,
            ]);
        ;

        $builder
            ->addEventListener(FormEvents::PRE_SET_DATA,
                function(FormEvent $event) use ($builder) {
                    $article = $event->getData();
                    if (!$article || null === $article->getId()) {
                        $event->getForm()->add('createdAt', DateTimeType::class, [
                            'required'      => true,
                            'input'         => 'datetime_immutable',
                            'label'         => 'app.actuality.created_at',
                            'date_widget'   => 'single_text',
                            'time_widget'   => 'single_text',
                            'required'      => false,
                            'data'          => new \DateTimeImmutable('now', new DateTimeZone('Europe/Paris')),
                        ])
                        ->add('updatedAt', DateTimeType::class, [
                            'required'      => true,
                            'input'         => 'datetime_immutable',
                            'label'         => 'app.actuality.updated_at',
                            'date_widget'   => 'single_text',
                            'time_widget'   => 'single_text',
                            'required'      => false,
                            'data'          => new \DateTimeImmutable('now', new DateTimeZone('Europe/Paris')),
                        ]);
                    } else {
                        $event->getForm()->add('createdAt', DateTimeType::class, [
                            'label'         => 'app.actuality.created_at',
                            'required'      => true,
                        ])
                        ->add('updatedAt', DateTimeType::class, [
                            'label'         => 'app.actuality.updated_at',
                            'data'          => new \DateTime('now', new \DateTimeZone('Europe/Paris')),
                            'required'      => true,
                        ]);
                    }
                }
            )
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'app_actuality_article';
    }
}
