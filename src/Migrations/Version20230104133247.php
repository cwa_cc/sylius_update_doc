<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230104133247 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE app_actuality_article (id INT AUTO_INCREMENT NOT NULL, enabled TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE actuality_article_actuality_category (actuality_article_id INT NOT NULL, actuality_category_id INT NOT NULL, INDEX IDX_B36266ABD429C7F5 (actuality_article_id), INDEX IDX_B36266AB268EF95F (actuality_category_id), PRIMARY KEY(actuality_article_id, actuality_category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_actuality_article_image (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) DEFAULT NULL, path VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_actuality_article_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, meta_description LONGTEXT NOT NULL, banner_title VARCHAR(255) NOT NULL, banner_alt VARCHAR(255) NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_1A14B67E2C2AC5D3 (translatable_id), UNIQUE INDEX app_actuality_article_translation_uniq_trans (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_actuality_category (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_actuality_category_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_BAA02D942C2AC5D3 (translatable_id), UNIQUE INDEX app_actuality_category_translation_uniq_trans (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE actuality_article_actuality_category ADD CONSTRAINT FK_B36266ABD429C7F5 FOREIGN KEY (actuality_article_id) REFERENCES app_actuality_article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE actuality_article_actuality_category ADD CONSTRAINT FK_B36266AB268EF95F FOREIGN KEY (actuality_category_id) REFERENCES app_actuality_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_actuality_article_translation ADD CONSTRAINT FK_1A14B67E2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES app_actuality_article (id)');
        $this->addSql('ALTER TABLE app_actuality_category_translation ADD CONSTRAINT FK_BAA02D942C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES app_actuality_category (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE actuality_article_actuality_category DROP FOREIGN KEY FK_B36266ABD429C7F5');
        $this->addSql('ALTER TABLE app_actuality_article_translation DROP FOREIGN KEY FK_1A14B67E2C2AC5D3');
        $this->addSql('ALTER TABLE actuality_article_actuality_category DROP FOREIGN KEY FK_B36266AB268EF95F');
        $this->addSql('ALTER TABLE app_actuality_category_translation DROP FOREIGN KEY FK_BAA02D942C2AC5D3');
        $this->addSql('DROP TABLE app_actuality_article');
        $this->addSql('DROP TABLE actuality_article_actuality_category');
        $this->addSql('DROP TABLE app_actuality_article_image');
        $this->addSql('DROP TABLE app_actuality_article_translation');
        $this->addSql('DROP TABLE app_actuality_category');
        $this->addSql('DROP TABLE app_actuality_category_translation');
    }
}
