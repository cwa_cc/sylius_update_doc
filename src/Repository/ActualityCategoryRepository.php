<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Page\Backend\ActualityCategory;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

/**
 * @extends ServiceEntityRepository<ActualityCategory>
 *
 * @method ActualityCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActualityCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActualityCategory[]    findAll()
 * @method ActualityCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActualityCategoryRepository extends EntityRepository
{
}
