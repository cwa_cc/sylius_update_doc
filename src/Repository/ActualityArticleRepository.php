<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Page\Backend\ActualityArticle;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Component\Core\Model\ChannelInterface;

/**
 * @extends ServiceEntityRepository<ActualityArticle>
 *
 * @method ActualityArticle|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActualityArticle|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActualityArticle[]    findAll()
 * @method ActualityArticle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActualityArticleRepository extends EntityRepository
{
    public function findLatestArticlesSortByDate(): array
    {
        return $this->createQueryBuilder('a')
            ->addOrderBy('a.updated_at', 'DESC')
            ->getQuery()
            ->getScalarResult();
    }

    // public function findLatestArticles(): array
    // {
    //     return $this->createQueryBuilder('a')
    //         ->addOrderBy('a.updated_at', 'DESC')
    //         ->setMaxResults(5)
    //         ->getQuery()
    //         ->getScalarResult();
    // }

    // public function findHomeLatestArticles(): array
    // {
    //     return $this->createQueryBuilder('a')
    //         ->addOrderBy('a.updated_at', 'DESC')
    //         ->setMaxResults(3)
    //         ->getQuery()
    //         ->getScalarResult();
    // }

    public function findHomeLatestArticles(string $locale): array
    {
        return $this->createQueryBuilder('o')
            ->addSelect('translation')
            ->innerJoin('o.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('o.enabled = :enabled')
            ->addOrderBy('o.updated_at', 'DESC')
            ->setParameter('locale', $locale)
            ->setParameter('enabled', true)
            ->setMaxResults(3)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findLatestFourArticles(string $locale): array
    {
        return $this->createQueryBuilder('o')
            ->addSelect('translation')
            ->innerJoin('o.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('o.enabled = :enabled')
            // ->andwhere('article != :exceptArticle')
            ->addOrderBy('o.updated_at', 'DESC')
            ->setParameter('locale', $locale)
            ->setParameter('enabled', true)
            ->setMaxResults(4)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findEnabledArticles(string $locale): array
    {
        return $this->createQueryBuilder('o')
            ->addSelect('translation')
            ->innerJoin('o.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('o.enabled = :enabled')
            ->addOrderBy('o.updated_at', 'DESC')
            ->setParameter('locale', $locale)
            ->setParameter('enabled', true)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findActualityIndexLatestArticles(string $locale): array
    {
        return $this->createQueryBuilder('o')
            ->addSelect('translation')
            ->innerJoin('o.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('o.enabled = :enabled')
            ->addOrderBy('o.updated_at', 'DESC')
            ->setParameter('locale', $locale)
            ->setParameter('enabled', true)
            ->setMaxResults(6)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findLatestArticles(string $locale): array
    {
        return $this->createQueryBuilder('o')
            ->addSelect('translation')
            ->innerJoin('o.translations', 'translation', 'WITH', 'translation.locale = :locale')
            ->andWhere('o.enabled = :enabled')
            ->addOrderBy('o.updated_at', 'DESC')
            ->setParameter('locale', $locale)
            ->setParameter('enabled', true)
            ->setMaxResults(5)
            ->getQuery()
            ->getResult()
        ;
    }
}
