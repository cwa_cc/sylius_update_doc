<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Page\Backend\ActualityArticleTranslation;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

/**
 * @extends ServiceEntityRepository<ActualityArticleTranslation>
 *
 * @method ActualityArticleTranslation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActualityArticleTranslation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActualityArticleTranslation[]    findAll()
 * @method ActualityArticleTranslation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActualityArticleTranslationRepository extends EntityRepository
{
}
