<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Page\Backend\ActualityCategoryTranslation;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

/**
 * @extends ServiceEntityRepository<ActualityCategoryTranslation>
 *
 * @method ActualityCategoryTranslation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActualityCategoryTranslation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActualityCategoryTranslation[]    findAll()
 * @method ActualityCategoryTranslation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActualityCategoryTranslationRepository extends EntityRepository
{
}
